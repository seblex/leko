<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m181130_121446_create_leko_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%leko}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string(32)->notNull()->unique(),
            'name' => $this->string(32)->notNull(),
            'lastname' => $this->string(32)->notNull(),
            'email' => $this->string(64)->notNull()->unique(),
            'password' => $this->string()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%leko}}');
    }
}
