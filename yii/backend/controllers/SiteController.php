<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\base\Exception;
use backend\components\Validator;
use backend\models\Leko;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return $this->render('login');
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionTest()
    {
        return $this->render('test');
    }

    public function actionRegister()
    {
        if (!Yii::$app->request->isGet ) {
            throw new Exception('Неверный тип запроса. Необходим GET!');
        }
        try {
            $query = Yii::$app->request->get();
            $v = new Validator();
            $v->setFields($query);
            $validate_result = $v->validate();
            if($validate_result['ok'] !== 'false'){
                $leko = new Leko();
                $leko->name = $query['name'];
                $leko->email = $query['lastname'];
                $leko->login = $query['login'];
                $leko->password = md5($query['password']);
                $leko->email = $query['email'];
                $leko->lastname = $query['lastname'];
                if ($leko->save()) {
                    $response = ['ok' => 'Пользователь успешно зарегистрирован'];
                } else {
                    $response = ['errors' => ['all' => 'Пользователь не зарегистрирован']];
                }
                return $this->asJson($response);
            } else {
                return $this->asJson($validate_result);
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

    }
}
