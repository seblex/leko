<?php

namespace backend\components;

use backend\models\Leko;

class Validator
{
    protected $name;
    protected $lastname;
    protected $login;
    protected $email;
    protected $password;
    protected $result;

    public function setFields($fields)
    {
        $this->name = $fields['name'];
        $this->email = $fields['email'];
        $this->lastname = $fields['lastname'];
        $this->login = $fields['login'];
        $this->password = $fields['password'];
    }

    public function validate()
    {
        $this->result = [
            'errors' => [
                'login' => '',
                'name' => '',
                'lastname' => '',
                'email' => '',
                'password' => ''
            ],
            'ok' => true
        ];
        $this->validateLogin();
        $this->validateName();
        $this->validateLastname();
        $this->validateEmail();
        $this->validatePassword();
        return $this->result;
    }

    protected function errors($key, $value)
    {
        $this->result['ok'] = 'false';
        $this->result['errors'][$key] = $value;
    }

    protected function validateLogin()
    {
        if (preg_match('/^[a-zA-Z0-9_]+$/i', $this->login)) {
            if (Leko::find()->where(['login' => $this->login])->one()) {
                $this->errors('login', 'Такой никнейм уже существует');
            }
        } else {
            $this->errors('login', 'Не корректный логин');
        }
    }

    protected function validateLastname()
    {
        if (preg_match('/[^а-я]+/msiu', $this->lastname)){
            $this->errors('lastname', 'Допустимы только русские буквы');
        }
    }

    protected function validateName()
    {
        if (preg_match('/[^а-я]+/msiu', $this->name)) {
            $this->errors('name', 'Допустимы только русские буквы');
        }
    }

    protected function validateEmail()
    {
        if (preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $this->email)) {
            if (Leko::find()->where(['email' => $this->email])->one()) {
                $this->errors('email', 'Такой адрес электронной почты уже существует');
            }
        } else {
            $this->errors('email', 'Не корректный адрес электронной почты');
        }
    }

    protected function validatePassword()
    {
        if (!strlen($this->password) > 5) {
            $this->errors('password', 'Пароль должен содержать не менее 5 символов');
        }
    }
}