<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Authentication asset bundle.
 */
class AuthAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/auth/register.css',
    ];
    public $js = [
        'js/auth/register.js',
    ];
    public $depends = ['backend\assets\AppAsset'];
    public $jsOptions = ['position' => View::POS_END];
}