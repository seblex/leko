<?php

/* @var $this yii\web\View */

\backend\assets\AuthAsset::register($this);

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h3>Регистрация:</h3>

    <div class="row">
        <div class="form-horizontal">
            <div class="form-group">
                <label for="login" class="col-sm-2 control-label">Никнейм</label>
                <div class="col-sm-1 input_result_icon" id="login_icon"></div>
                <div class="col-sm-5">
                    <input type="text" class="form-control js_input_login" id="login" data-icon="login">
                </div>
                <div class="col-sm-4 login_error_text error_text"></div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Имя</label>
                <div class="col-sm-1 input_result_icon" id="name_icon"></div>
                <div class="col-sm-5">
                    <input type="text" class="form-control js_input_name" id="name" data-icon="name">
                </div>
                <div class="col-sm-4 name_error_text error_text"></div>
            </div>
            <div class="form-group">
                <label for="lastname" class="col-sm-2 control-label">Фамилия</label>
                <div class="col-sm-1 input_result_icon" id="lastname_icon"></div>
                <div class="col-sm-5">
                    <input type="text" class="form-control js_input_lastname" id="lastname" data-icon="lastname">
                </div>
                <div class="col-sm-4 lastname_error_text error_text"></div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Электронная почта</label>
                <div class="col-sm-1 input_result_icon" id="email_icon"></div>
                <div class="col-sm-5">
                    <input type="text" class="form-control js_input_email" id="email" data-icon="email">
                </div>
                <div class="col-sm-4 email_error_text error_text"></div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Пароль</label>
                <div class="col-sm-1 input_result_icon" id="password_icon"></div>
                <div class="col-sm-5">
                    <input type="password" class="form-control js_input_password" id="password" data-icon="password">
                </div>
                <div class="col-sm-4 password_error_text error_text"> </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" class="btn btn-default disabled js_send_form">Готово</button>
                </div>
            </div>
        </div>

    </div>
</div>
