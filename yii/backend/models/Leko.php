<?php

namespace backend\models;

/**
 * This is the model class for table "leko".
 *
 * @property integer $id
 * @property string $name
 * @property string $lastname
 * @property string $login
 * @property string $email
 * @property string $password
 *
 */

class Leko extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%leko}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'name', 'lastname', 'email', 'password'], 'required'],
            [['login', 'name', 'lastname', 'password'], 'string', 'max'=>32],
            [['email'], 'string', 'max'=>64],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Никнейм',
            'name' => 'Имя',
            'lastname' => 'Фамилия',
            'email' => 'Адрес электронной почты',
            'password' => 'Пароль',
        ];
    }

}