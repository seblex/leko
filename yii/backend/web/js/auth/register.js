//Валидация поля с логином
$(document).on('change', '.js_input_login', function(e) {
    var text = e.target.value;
    var icon = e.target.dataset.icon;

    if (text.match(/^[a-zA-Z0-9_]+$/i)) {
        ok(icon);
    } else {
        fake(icon);
    }

    activate_button();
});

//Валидация поля с именем
$(document).on('change', '.js_input_name', function(e) {
    var text = e.target.value;
    var icon = e.target.dataset.icon;

    if (text.match(/^[а-яА-Я_]+$/i)) {
        ok(icon);
    } else {
        fake(icon);
    }
});

//Валидация поля с фамилией
$(document).on('change', '.js_input_lastname', function(e) {
    var text = e.target.value;
    var icon = e.target.dataset.icon;

    if (text.match(/^[а-яА-Я_]+$/i)) {
        ok(icon);
    } else {
        fake(icon);
    }
});

//Валидация поля с электронной почтой
$(document).on('change', '.js_input_email', function(e) {
    var text = e.target.value;
    var icon = e.target.dataset.icon;

    if (text.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
        ok(icon);
    } else {
        fake(icon);
    }
});

//Валидация поля с паролем
$(document).on('change', '.js_input_password', function(e) {
    var text = e.target.value;
    var icon = e.target.dataset.icon;

    if (text.length > 5) {
        ok(icon);
    } else {
        fake(icon);
    }
});

//Удачная валидация
function ok(icon){
    icon_ok(icon);
    hide_error_text(icon);
    valid_on(icon);
    activate_button();
}

//Неудачная валидация
function fake(icon){
    show_error_text(icon);
    icon_false(icon);
    valid_off(icon);
    activate_button();
}

//Тексты ошибок
var error_texts = {
    'login': 'Не коррктный логин',
    'name': 'Допустимы только русские буквы',
    'lastname': 'Допустимы только русские буквы',
    'email': 'Не корректный адрес электронной почты',
    'password': 'Пожалуйста придумайте пароль длиннее 5 символов'
};

//Результаты валидации полей
var valid_fields = {
    'login': false,
    'name': false,
    'lastname': false,
    'email': false,
    'password': false
}

//Простановка удачной валидации поля
function valid_on(icon){
    valid_fields[icon] = true;
}

//Простановка неудачной валидации поля
function valid_off(icon){
    valid_fields[icon] = false;
}

//активация кнопки отправки формы
function activate_button(){
    var activate = true;
    for (var item in valid_fields){
        if (valid_fields[item] == false) {
            activate = false;
        }
    }
    if (activate == true) {
        $('.js_send_form').removeClass('disabled');
    } else {
        $('.js_send_form').addClass('disabled');
    }
}

//Показать текст ошибки
function show_error_text(icon){
    $('.' + icon + '_error_text').css('display', 'block');
    $('.' + icon + '_error_text').html(error_texts[icon]);
}

//Скрыть текст ошибки
function hide_error_text(icon){
    $('.' + icon + '_error_text').css('display', 'none');
    $('.' + icon + '_error_text').html('');
}

//Показ иконки удачной валидации
function icon_ok(icon){
    $('#' + icon + '_icon').css('color', 'green');
    $('#' + icon + '_icon').html('<i class="glyphicon glyphicon-ok"></i>');
}

//показ иконки неудачной валидации
function icon_false(icon){
    $('#' + icon + '_icon').css('color', 'red');
    $('#' + icon + '_icon').html('!');
}

//Отправка формы на сервер
$(document).on('click', '.js_send_form', function(e){
    var data = {
        'login': $('#login').val(),
        'name': $('#name').val(),
        'lastname': $('#lastname').val(),
        'email': $('#email').val(),
        'password': $('#password').val(),
    };
    console.log(data);
    $.ajax({
        url: '/site/register',
        type: 'GET',
        data: data,
        success: function(data){
            if (data.ok !== 'false') {
                $('.form-horizontal').detach();
                $('.form_container').html('<div class="alert alert-success" role="alert">' + data.ok + '</div>');
            } else {
                if (data.errors.all) {
                    console.log('Ошибка при сохранении - надо фиксить баг, чтобы такого не случалось. Ибо пользователю такие ошибки показываться не должны!.');
                } else {
                    console.log(3468756);
                    for (var i in data.errors) {
                        if (data['errors'][i] != '') {
                            fake(i);
                            $('.' + i + '_error_text').html(data.errors[i]);
                        }
                    }
                }
            }
        },
        error: function(error){
            console.log(error);
        }
    });
});